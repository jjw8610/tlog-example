package com.yomahub.tlog.example.feign;

import com.yomahub.tlog.core.enhance.bytes.AspectLogEnhance;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
public class TLogFeignConsumerRunner {

//    static {AspectLogEnhance.enhance();}

    public static void main(String[] args) {
        SpringApplication.run(TLogFeignConsumerRunner.class, args);
    }
}
