package com.yomahub.tlog.example.dubbo.service.impl;

import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSON;
import com.yomahub.tlog.core.annotation.TLogAspect;
import com.yomahub.tlog.example.dubbo.service.DemoService;
import com.yomahub.tlog.example.vo.User;
import com.yomahub.tlog.example.vo.UserDetail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service("demoService")
public class DemoServiceImpl implements DemoService {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private AspectDomain aspectDomain;

    @Override
    public String sayHello(String name) {
        log.info("logback-dubbox-provider:invoke method sayHello,name={}", name);

        new AsynDomain().start();

        User user = initUserData();
        String userJson = JSON.toJSONString(user);
        aspectDomain.test(102, "i am a good people", userJson);

        return "hello," + name;
    }

    private User initUserData() {
        UserDetail userDetail = new UserDetail(102L, "上海牛逼有限公司", "上海南京路1000号", DateUtil.parseDate("1950-02-02"));
        User user = new User(102L, "张三", 30, userDetail);
        return user;
    }
}