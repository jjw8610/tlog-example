package com.yomahub.tlog.example.vo;

import java.util.Date;

public class UserDetail {

    private Long id;

    private String company;

    private String address;

    private Date birthday;

    public UserDetail(Long id, String company, String address, Date birthday) {
        this.id = id;
        this.company = company;
        this.address = address;
        this.birthday = birthday;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }
}
