package com.yomahub.tlog.example.log4j2.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class DemoController {
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @RequestMapping("/hi")
    public String sayHello(@RequestParam String name){
        log.info("invoke demo method sayHello");
        return "hello,"+name;
    }

    @RequestMapping("/sayth")
    public String saySth(HttpServletRequest request){
        log.info(request.getHeader("abc"));
        return "hello";
    }
}
