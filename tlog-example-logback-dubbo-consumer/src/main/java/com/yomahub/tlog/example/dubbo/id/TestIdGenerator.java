package com.yomahub.tlog.example.dubbo.id;

import com.yomahub.tlog.id.TLogIdGenerator;

public class TestIdGenerator implements TLogIdGenerator {
    @Override
    public String generateTraceId() {
        return String.valueOf(System.nanoTime());
    }
}
