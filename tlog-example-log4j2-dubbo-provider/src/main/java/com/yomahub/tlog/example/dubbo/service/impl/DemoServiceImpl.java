package com.yomahub.tlog.example.dubbo.service.impl;

import com.yomahub.tlog.example.dubbo.service.DemoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service("demoService")
public class DemoServiceImpl implements DemoService {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public String sayHello(String name) {
        log.info("dubbox-provider:invoke method sayHello,name={}",name);
        new AsynDomain().start();
        return "hello," + name;
    }
}
